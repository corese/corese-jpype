import atexit
import jpype
import jpype.imports
from jpype.types import *

# Launch the JVM
#jpype.startJVM(classpath=['/fr/inria/corese/corese-core/4.5.0/corese-core-4.5.0-jar-with-dependencies.jar'])
jpype.startJVM()

def exit_handler():
    jpype.shutdownJVM()
    print('\n' * 2)
    print('JVM stopped')


atexit.register(exit_handler)

# Import of class
from fr.inria.corese.core import Graph
from fr.inria.corese.core.load import Load
from fr.inria.corese.core.transform import Transformer
from fr.inria.corese.core.query import QueryProcess
from fr.inria.corese.core.logic import RDF

###############
# Build Graph #
###############


def BuildGraphCoreseApi():
    """Build a graph with a single statement (Edith Piaf is a singer) with the Corese API
    :returns: graph with a single statement (Edith Piaf is a singer)
    """
    corese_graph = Graph()

    # NameSpace
    ex = "http://example.org/"

    # Create and add statement: Edith Piaf is an Singer
    edith_Piaf_IRI = corese_graph.addResource(ex + "EdithPiaf")
    rdf_Type_Property = corese_graph.addProperty(RDF.TYPE)
    singer_IRI = corese_graph.addResource(ex + "Singer")

    corese_graph.addEdge(edith_Piaf_IRI, rdf_Type_Property, singer_IRI)

    return corese_graph

##########
# Sparql #
##########


def sparqlQuery(graph, query):
    """Run a query on a graph

    :param graph: the graph on which the query is executed
    :param query: query to run
    :returns: query result
    """
    exec = QueryProcess.create(graph)
    return exec.query(query)


#################
# Load / Export #
#################


def exportToFile(graph, format, path):
    """Export a graph to a file

    :param graph: graph to export
    :param format: format of export
    :param path: path of the exported file
    """
    transformer = Transformer.create(graph, format)
    transformer.write(path)


def load(path):
    """Load a graph from a local file or a URL

    :param path: local path or a URL
    :returns: the graph load
    """
    graph = Graph()

    ld = Load.create(graph)
    ld.parse(path)

    return graph


########
# Main #
########


def printTitle(title):
    title = "== " + title + " =="
    border = "=" * len(title)
    print("\n" * 2)
    print(border)
    print(title)
    print(border)


###
# Build a graph with the Corese API
###
printTitle("Build a graph with the Corese API")

graph = BuildGraphCoreseApi()
print("Graph build ! (" + str(graph.size()) + " triplets)")

print("\nPrint Graph:")
print(graph.display())


###
# SPARQL Query
###
printTitle("SPARQL Query")

graph = load(
    "https://raw.githubusercontent.com/stardog-union/stardog-tutorials/master/music/beatles.ttl")
print("Graph load ! (" + str(graph.size()) + " triplets)")

# List of U2 albums
query = """
        prefix : <http://stardog.com/tutorial/>

        SELECT ?member 
        WHERE {
            ?The_Beatles :member ?member
        }
        """

map = sparqlQuery(graph, query)
print("\nQuery result ! (List of members of bands \"The Beatles\"): ")
print(map)


###
# Load / Export
###
printTitle("Load / Export")

graph = load(
    "https://raw.githubusercontent.com/stardog-union/stardog-tutorials/master/music/beatles.ttl")
print("Graph load ! (" + str(graph.size()) + " triplets)")

path_export_file = "export.rdf"
exportToFile(graph, Transformer.RDFXML, path_export_file)
print("Graph Export in file (" + path_export_file + ")")
