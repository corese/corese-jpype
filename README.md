# corese-jpype


## Purpose

Provide a minimalistic python access to corese-core.

Since it relies on conda, maven and java, it should work on most platforms.

## Installation
### Tool Prerequisite

1. git 
2. A conda client, e.g. [Miniconda3](https://docs.anaconda.com/free/miniconda/miniconda-install/).

```bash
git clone git@gitlab.inria.fr:corese/corese-jpype.git
cd corese-jpype
PROJECT_ROOT=$PWD
conda env update --prune -f pkg/env/corese.yaml
conda activate corese
mvn dependency:get -DgroupId=fr.inria.corese -DartifactId=corese-core -Dversion=4.5.0 -Dmaven.repo.local=$CONDA_PREFIX/.m2 -Dclassifier=jar-with-dependencies
export CLASSPATH=$CONDA_PREFIX/.m2/fr/inria/corese/corese-core/4.5.0/corese-core-4.5.0-jar-with-dependencies.jar
python src/example1.py
```
